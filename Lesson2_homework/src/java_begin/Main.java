package java_begin;


public class Main {

    public static void main(String[] args) throws MyArraySizeException, MyArrayDataException {
        String[][] array = {{"n", "5", "6", "4"}, {"2", "3", "2", "3"}, {"1", "2", "1", "2"}, {"1", "2","5", "2"}};
        System.out.println(method(array));
    }


    public static class MyArraySizeException extends Exception {
        public MyArraySizeException(String msg) {

            super(msg);
        }

    }

    public static class MyArrayDataException extends Exception {
        private int row;
        private int col;

        public MyArrayDataException(String message, int row, int col) {
            super(message + row + col);
            this.col = col;
            this.row = row;
        }
    }

    public static int method(String[][] array) throws MyArraySizeException, MyArrayDataException {

        int sum = 0;
        if (array.length != 4) throw new MyArraySizeException("Размерность массива должна быть [4 x 4]\n");
        for (int i = 0; i < array.length; i++) {
            if (array[i].length != 4) throw new MyArraySizeException("Размерность массива должна быть [4 x 4] \n");
            for (int j = 0; j < array.length; j++) {
                try {
                    sum += Integer.parseInt(array[i][j]);
                } catch (NumberFormatException e) {
                    throw new MyArrayDataException("Неверные данные находятся в ячейке ", i, j);
                }
            }
        }
        return sum;
    }
}


