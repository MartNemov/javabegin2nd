import java.util.*;

public class Main {
    public static void main(String[] args) {

        /// Задачи 1,2
        shift();
        transformArrayToList();

        ///Задача 3
        Apple apple1 = new Apple();
        Apple apple2 = new Apple();
        Apple apple3 = new Apple();

        Orange orange1 = new Orange();
        Orange orange2 = new Orange();

        Box<Apple> box1 = new Box<>(apple1, apple2, apple3);
        Box<Orange> box2 = new Box<>(orange1, orange2);

        System.out.println(box1.compare(box2));

        Box<Orange> box3 = new Box<>();
        box2.transfer(box3);
    }


    static void shift() {
        int[] array = {1, 2, 3, 4, 5};
        int a = array[0];
        array[0] = array[4];
        array[4] = a;

        for (int j : array) {
            System.out.print(j + " ");
        }
        System.out.println();
    }

    static void transformArrayToList() {
        Integer[] arr = {101, 202, 303, 404};
        List<Integer> list = new ArrayList<>(Arrays.asList(arr));
        System.out.println(list);
    }
}
