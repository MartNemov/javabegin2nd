public abstract class Fruit {
    public Fruit(float weight) {
        this.weight = weight;
    }

    protected float weight;

    public float getWeight() {
        return weight;
    }
}
