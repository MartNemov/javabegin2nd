import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        readFile();
        stitchFiles();
        readingTxt();
    }

    private static void readFile() {
        try (FileInputStream fis = new FileInputStream("0file")) {
            byte[] array = new byte[fis.available()];
            fis.read(array, 0, fis.available());
            for (int i = 0; i < array.length; i++) {
                System.out.print((char) array[i]);
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static void stitchFiles() {
        try {
            ArrayList<InputStream> al = new ArrayList<>();
            al.add(new FileInputStream("1file"));
            al.add(new FileInputStream("2file"));
            al.add(new FileInputStream("3file"));
            al.add(new FileInputStream("4file"));
            al.add(new FileInputStream("5file"));

            Enumeration<InputStream> en = Collections.enumeration(al);
            SequenceInputStream sis = new SequenceInputStream(en);

            FileOutputStream fos = new FileOutputStream("stitch");

            int data;
            while ((data = sis.read()) != -1) {
                fos.write(data);
            }
            sis.close();
            fos.close();
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static void readingTxt() {
        final int size = 1800;
        try (RandomAccessFile raf = new RandomAccessFile("D:\\RandomJavaStart\\homework3\\book.docx", "r")) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter page number:");
            int page = sc.nextInt();
            raf.seek(page * size);
            byte[] buffer = new byte[1800];
            raf.read(buffer);
            if (page <= 421) {
                System.out.print(new String(buffer));
            } else {
                System.out.print("Book was ended");
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
