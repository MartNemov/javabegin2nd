package java_begin;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ServerSocket server = null;
        Socket socket = null;

        try {
            server = new ServerSocket(8189);
            System.out.println("server start");

            socket = server.accept();
            System.out.println("client add");

            Scanner in = new Scanner(socket.getInputStream());
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

            while (true) {
                String str = in.nextLine();
                System.out.println("Client " + str);
                if (str.equals("/end")) {
                    break;
                }
                out.println("echo " + str);
            }

            while (true) {

            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    server.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }
}

