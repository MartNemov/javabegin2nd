import org.junit.Assert;
import org.junit.Test;

public class ArraysTest {
    Main main = new Main();

    @Test
    public void testPullarray() {
        Assert.assertArrayEquals(new int[]{1, 7}, main.pullArray(1, 2, 4, 4, 2, 3, 4, 1, 7));
    }

    @Test
    public void testPullarray1() {
        Assert.assertArrayEquals(new int[]{2, 3, 5, 1, 7}, main.pullArray(4, 4, 4, 4, 2, 3, 5, 1, 7));
    }

    @Test
    public void testPullarray2() {
        Assert.assertArrayEquals(new int[]{8, 9, 7}, main.pullArray(1, 2, 3, 4, 2, 4, 8, 9, 7));
    }

    @Test
    public void testsearchElements() {
        Assert.assertTrue(main.searchElements(new int[]{1, 1, 1, 4, 4, 1, 4, 4}));
    }

    @Test
    public void testsearchElements1() {
        Assert.assertTrue(main.searchElements(new int[]{1, 1, 1, 1, 1, 1}));
    }

    @Test
    public void testsearchElements2() {
        Assert.assertTrue(main.searchElements(new int[]{4, 4, 4, 4}));
    }

    @Test
    public void testsearchElements3() {
        Assert.assertTrue(main.searchElements(new int[]{1, 4, 4, 1, 1, 4, 3}));
    }
}
