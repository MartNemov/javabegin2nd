public class Main {
    public static void main(String[] args) {
        System.out.println("Create methods for the testing code");
    }

    public static int[] pullArray(int... array) throws RuntimeException {
        if (array.length == 0) {
            throw new RuntimeException("Array is empty!");
        } else {
            for (int i = array.length - 1; i >= 0; i--) {
                if (array[i] == 4) {
                    int[] newArray = new int[array.length - i - 1];
                    for (int j = 0; j < array.length - i - 1; j++) {
                        newArray[j] = array[i + j + 1];
                    }
                    return newArray;
                }
            }
        }
        throw new RuntimeException("Array does not contain 4!");
    }

    public static boolean searchElements(int... array) throws RuntimeException {
        if (array.length == 0) {
            throw new RuntimeException("Array is empty!");
        } else {
            int val1 = 1;
            int val2 = 4;
            boolean one = false;
            boolean four = false;
            for (int i : array) {
                if (i == val1) one = true;
                else if (i == val2) four = true;
                else return false;
            }
            return one && four;
        }
    }
}

