package Spring_1;

import org.springframework.stereotype.Component;

@Component("Inform")
public class Inform_MedicalCard implements Inform {

    @Override
    public void medicalCardIssue() {
        System.out.println("Возьмите свою медицинскую карту");
    }
}

