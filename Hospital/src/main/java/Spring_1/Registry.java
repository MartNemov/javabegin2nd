package Spring_1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Component("Registry")
public class Registry {
    @Autowired
    private Inform inform;

    public Inform getInform() {
        return inform;
    }

    public void setInform(Inform inform) {
        this.inform = inform;
    }

    public void officeNumber() {
        Map<String, Integer> docs = new HashMap<>();
        docs.put("Therapist", 100);
        docs.put("Cardiologist", 101);
        docs.put("Dentist", 102);
        Random random = new Random();
        Object[] val = docs.values().toArray();
        Integer randomValue = (Integer) val[random.nextInt(val.length)];
        inform.medicalCardIssue();
        System.out.println("Ваш доктор находится в кабинете № " + randomValue);
    }

}

