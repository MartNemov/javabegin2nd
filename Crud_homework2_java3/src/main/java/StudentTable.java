import java.sql.*;

public class StudentTable {
    private static Statement statement;

    public static void main(String[] args) {
        try {
            connectDatabase();
            addTable();
            addEntry();
            getTable();
            deleteRow();
            dropTable();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void connectDatabase() throws Exception {
        Class.forName("org.sqlite.JDBC");
        Connection conn = DriverManager.getConnection(
                "jdbc:sqlite:C:\\Users\\Март Немов\\Desktop\\MainTable.db");
        statement = conn.createStatement();
        System.out.println("Connected");
    }

    private static void addTable() throws SQLException {
        String addTable = "CREATE TABLE students (Id INTEGER PRIMARY KEY AUTOINCREMENT, Name VARCHAR(20) UNIQUE, Grade INT)";
        statement.executeUpdate(addTable);
        System.out.println("Database has been created");
    }

    private static void addEntry() throws SQLException {
        String addEntry = "INSERT INTO students (Name , Grade) VALUES ('Ivan Ivanov', 5), ('Fedor Fedorov', 3), ('Sergey Sergeev', 4)";
        statement.executeUpdate(addEntry);
        System.out.println("Entries added");
    }

    private static void getTable() throws SQLException {
        ResultSet resultSet = statement.executeQuery("SELECT * FROM Students");
        while (resultSet.next()) {
            String name = resultSet.getString("Name");
            int grade = resultSet.getInt("Grade");
            System.out.printf("Student " + "%s" + " received a grade of " + "%d \n", name, grade);
        }
    }

    private static void deleteRow() throws SQLException {
        int deleteRow = statement.executeUpdate("DELETE FROM Students WHERE id = 1");
        System.out.printf("The student with Id %d has been removed from Table", deleteRow);
        System.out.println();
    }

    private static void dropTable() throws SQLException {
        String dropTable = "DROP Table Students";
        statement.executeUpdate(dropTable);
        System.out.println("Table deleted in given database");
    }
}
