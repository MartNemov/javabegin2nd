package Competitors;


public class Team {
    String title;

    public Team(String title, Competitor... runners) {
        this.title = title;
        this.runners = runners;
    }

    private Competitor[] runners;

    public Competitor[] getRunners() {
        return runners;
    }

    public void showWinners() {
        System.out.println("win");
        for (Competitor o : runners) {
            if (o.isOnDistance()) {
                o.info();
            }
        }
    }

}
