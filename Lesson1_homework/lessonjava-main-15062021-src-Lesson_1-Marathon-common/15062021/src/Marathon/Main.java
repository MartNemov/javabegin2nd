package Marathon;

import Competitors.Cat;
import Competitors.Dog;
import Competitors.Human;
import Competitors.Team;
import Terms.Course;
import Terms.Cross;
import Terms.Wall;

public class Main {
    public static void main(String[] args) {
        Team team = new Team("Rocket", new Human("Bob"), new Cat("Vaska"), new Dog("Bobik"));
        Course course = new Course(new Cross(80), new Wall(2), new Wall(1), new Cross(120));
        course.doIt(team);
        team.showWinners();
        System.out.println("Check");


    }
}
