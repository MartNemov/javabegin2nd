package Terms;

import Competitors.Competitor;

public abstract class Obstacle {
    public abstract void doIt(Competitor competitor);
}

