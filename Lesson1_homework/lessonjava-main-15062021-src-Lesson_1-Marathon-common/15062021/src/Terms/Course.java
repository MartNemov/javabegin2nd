package Terms;

import Competitors.Competitor;
import Competitors.Team;

public class Course {
    private Obstacle[] block;

    public Course(Obstacle... block) {
        this.block = block;
    }

    public void doIt(Team team) {
        for (Competitor c : team.getRunners()) {
            for (Obstacle o : block) {
                o.doIt(c);
                if (!c.isOnDistance()) break;
            }
        }
    }


}
